import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Games from '../views/Games.vue';

import store from '../store/index';

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/games',
    name: 'Games',
    component: Games,
    beforeEnter: (to, from, next) => {
      if(store.getters.isAuthenticated)
        next();
      else{
        next({
          path:'/login',
          query: {
            loggedIn: false,
          }
        });
      }
    },
    children: [
      {
        path: 'slots',
        name: 'Slots',
        
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/Slots.vue')
      },
      {
        path: 'poker',
        name: 'Poker',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/Poker.vue')
      },
      {
        path: 'deposit',
        name: 'Deposit',
        component: () => import('../views/Deposit.vue')
      }
    ],
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});



export default router
